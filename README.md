# **TCP/IP Communication between two Raspberry Pies**

### **System Diagram**

![](Basic_Comunication.png)

### **Project Description**

This project is about communication between two Raspberry Pies using TCP/IP sockets. One Raspberry Pi sends information about its CPU temperature to gateway(my personal computer). The Gateway send the temperature informatio to the other gateway(Virtual Machine) which send the data to the second Raspberry (Virtual Machine). Based on the data the Raspberry calculates a PWM value and sends it back to the first Raspberry.

### **Software Logic**

![](SoftwareLogic.png)

### **Link to Youtube Video Demonstration**

https://youtu.be/fKhE7Wjvm9g 