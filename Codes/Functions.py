#!/usr/bin/env python

import subprocess


def get_temp():
    """Get the core temperature.
    Run a shell script to get the core temp and parse the output.
    Raises:
        RuntimeError: if response cannot be parsed
    Returns:
        float: The core temperature in degrees Celsius
    """

    output = subprocess.run(['vcgencmd', 'measure_temp'], capture_output=True)
    temp_str = output.stdout.decode()

    try:
        return float(temp_str.split('=')[1].split('\'')[0])
    except (IndexError, ValueError):
        raise RuntimeError('Could not parse temperature output.')


# ==================================================================================================

def upload_data(table, data, temp):
    from firebase import firebase

    firebase = firebase.FirebaseApplication("https://raspberrypi-temperature-9d53a.firebaseio.com/",
                                            None)  # make connection

    data = {  # write the data
        data : temp
    }

    firebase.post(table, data)  # store the data in a table called 'RPi-Temp'

    # id = result['name']

    # print(id)
    # return id


# ==================================================================================================================

def download_data(table, data):
    from firebase import firebase

    firebase = firebase.FirebaseApplication("https://raspberrypi-temperature-9d53a.firebaseio.com/", None)  # connect
    result = firebase.get(table, '')  # reach the data

    key1 = 0

    for key in result:  # get the key
        key1 = key
        # print(key1)

    temp = result[key1][data]  # store the data

    firebase.delete(table, '')  # delete the modest data

    return temp
