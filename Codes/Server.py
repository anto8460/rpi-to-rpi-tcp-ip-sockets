#!/usr/bin/env python

import socket
import Functions
from time import sleep
from gpiozero import PWMLED

GPIO_PIN = 17              # Set the controlling pin
fan = PWMLED(GPIO_PIN)     # Configure it for PWM


TCP_IP      = '169.254.158.77'
TCP_PORT    = 5005
BUFFER_SIZE = 1024

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # Creating a Socket
s.bind((TCP_IP, TCP_PORT))
s.listen(1)

conn, addr = s.accept()


while True:

        temper   = Functions.get_temp()  # Store the temperature
        temp_str = str(temper)           # Convert it to string

        conn.send(temp_str.encode())     # Send the string encoded

        data = conn.recv(BUFFER_SIZE)    # Receive the PWM
        fan.value = int(data.decode())   # turn the fan on


        #sleep(5)

s.close()
