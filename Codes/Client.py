#!/usr/bin/env python
import Functions
import socket
from time import sleep

TCP_IP      = '169.254.158.77'
TCP_PORT    = 5005
BUFFER_SIZE = 1024


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_IP, TCP_PORT))

while True:
    data = s.recv(BUFFER_SIZE)                      # Receive the temperature
    print('Temperature: ', str(data.decode()))      # Print it
    temp = data.decode()                            # Store it in the variable
    Functions.upload_data('Temperature', 'Degrees', temp)  # Upload the temperature
    #sleep(60)
    while True:
        try:
            PWM = Functions.download_data('PWM', 'Duty_Cycle')
            s.send(PWM.encode())
            break
        except:
            continue

s.close()

