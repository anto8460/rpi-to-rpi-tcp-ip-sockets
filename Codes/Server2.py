#!/usr/bin/env python

import socket
import Functions
from time import sleep

TCP_IP      = '169.254.158.77'
TCP_PORT    = 5004
BUFFER_SIZE = 1024

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # Creating a Socket
s.bind((TCP_IP, TCP_PORT))
s.listen(1)

conn, addr = s.accept()

while True:

    while True:
        try:
            temperature = Functions.download_data('Temperature', 'Degrees')
            break
        except:
            continue

    conn.send(temperature.encode())


    data = conn.recv(BUFFER_SIZE)
    PWM = data.decode()

    Functions.upload_data('PWM', 'Duty_Cycle', PWM)
