#!/usr/bin/env python
import Functions
import socket

TCP_IP      = '169.254.158.77'
TCP_PORT    = 5005
BUFFER_SIZE = 1024

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_IP, TCP_PORT))

while True:
    data = s.recv(BUFFER_SIZE)                      # Receive the temperature

    print('Temperature: ', str(data.decode()))      # Print it

    while True:
        try:
            temp = float(data.decode())
            break
        except ValueError:
            continue

    if temp > 40:
        PWM = '1'
    elif temp < 35:
        PWM = '0'
    elif temp > 35:
        PWM = '0.8'
    else:
        break

    s.send(PWM.encode())
    #Functions.upload_data('PWM', 'Duty_Cycle', )